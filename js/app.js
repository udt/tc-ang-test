(function () {
    'use strict';
    var app = angular.module('myApp', ['ngRoute']);

    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.when('/users', {
                templateUrl: 'partials/user-list.html',
                controller: 'UserController'
            }).when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'LoginController'
            }).otherwise({
                redirectTo: '/login'
            });
        }]);

    app.controller('UserController', ['$scope', '$location', function UserController($scope, $location) {
        var demoUsers = [
            {
                name: 'User 1'
            },
            {
                name: 'User 2'
            }];

        var adminUsername = localStorage.getItem('adminUsername');
        if (adminUsername === null) {
            $location.path('/login');
        }

        $scope.savedUsers = localStorage.getItem('users');
        $scope.adminUsername = adminUsername;

        $scope.users = (localStorage.getItem('users') !== null) ? JSON.parse($scope.savedUsers) : demoUsers;
        localStorage.setItem('users', JSON.stringify($scope.users));

        $scope.addUser = function () {

            $scope.users.push({
                name: $scope.name
            });

            $scope.name = '';

            localStorage.setItem('users', JSON.stringify($scope.users));
        };

        $scope.archive = function () {
            var oldusers = $scope.users;
            $scope.users = [];
            angular.forEach(oldusers, function (user) {
                if (!user.isSelected)
                    $scope.users.push(user);
            });
            localStorage.setItem('users', JSON.stringify($scope.users));
        };
        $scope.logout = function () {
            localStorage.setItem('adminUsername', null);
            $location.path('/login');
        };
    }]);

    app.controller('LoginController', ['$scope', '$location', function ($scope, $location) {
        $scope.username = '';
        $scope.login = function () {
            if ($scope.password === 'demo') {
                localStorage.setItem('adminUsername', $scope.username);
                $location.path("/users");
            } else {
                localStorage.setItem('adminUsername', null);
            }
        }

    }]);


})();
